import passport from "passport"
import passportGithub from "passport-github2"
import passportTwitter from "passport-twitter"
import jws from "jws"
import cookieParser from "cookie-parser"
import cookieSession from "cookie-session"
import Url from "url"

class User {
  constructor(obj){
    this.id = obj.id
    this.username = obj.username
    this.avatarUrl = obj.avatarUrl
  }
}
class GitHubUser extends User {
  constructor(profile){
    super({
      id: `github-${profile.id}`,
      username: profile.username,
      avatarUrl: profile._json.avatar_url
    })
  }
}
class TwitterUser extends User {
  constructor(profile){
    super({
      id: `twitter-${profile.id}`,
      username: profile.username,
      avatarUrl: profile._json.profile_image_url_https
    })
  }
}
const AuthEndpoints = (app, io, config) => {
  app.use(passport.initialize())
  app.use(passport.session())
  passport.serializeUser((user, done) => {
    let signature = jws.sign({
      header: {alg: "HS256"}
      , payload: user
      , secret: config.SECRET
    })
    done(null, signature)
  })

  passport.deserializeUser((token, done) => {
    let decodedSignature = jws.decode(token)
    if(!decodedSignature) return done(null, null)
    done(null, JSON.parse(decodedSignature.payload))
  })
  
  passport.use(new passportGithub.Strategy({
      clientID: config.GITHUB_CLIENT_ID,
      clientSecret: config.GITHUB_CLIENT_SECRET,
      callbackURL: config.GITHUB_CALLBACK_URL
    }, (accessToken, refreshToken, profile, done) => {
      console.log("from github strategy")
      return done(null, new GitHubUser(profile))
    }
  ))
  passport.use(new passportTwitter.Strategy({
      consumerKey: config.TWITTER_CONSUMER_KEY,
      consumerSecret: config.TWITTER_CONSUMER_SECRET,
      callbackURL: config.TWITTER_CALLBACK_URL
    }, (accessToken, refreshToken, profile, done) => {
      console.log("from twitter strategy", profile)
      return done(null, new TwitterUser(profile))
    }
  ))
  
  app.get("/login/github", passport.authenticate("github"))
  app.get("/auth/callbacks/github", passport.authenticate("github", {successRedirect: "/rooms/general", failureRedirect: "/"}))
  app.get("/login/twitter", passport.authenticate("twitter"))
  app.get("/auth/callbacks/twitter", passport.authenticate("twitter", {successRedirect: "/rooms/general", failureRedirect: "/"}))
  app.get("/logout", (req, res)=>{
    req.logout()
	  res.redirect("/")
  })
  app.use((req, res, next)=>{
    if(["rooms"].some(key => req.url.indexOf(key) > -1) && !req.isAuthenticated()){
      console.error("User is not authed")
      return next(401)
    }
    next()
  })
  io.set("authorization", (req, callback)=>{
    const err = null
    const authorized = true // just to show the signature
    app.cookieParser(req, req.res, () => {
      app.cookieSession(req, req.res, () => {
        const userString = req.session.passport ? req.session.passport.user : null
        const decodedSignature = jws.decode(userString)
        if(!decodedSignature){
          console.error(userString, "Unauthed")
          return callback(err, false)
        }
        req.user = JSON.parse(decodedSignature.payload)
        callback(err, true)
      })
    })
  })
}

export default AuthEndpoints
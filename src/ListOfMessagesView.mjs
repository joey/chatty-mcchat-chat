import MakeObservableList from "./MakeObservableList.mjs"
const SpeakerView = container => {
  const ol = container.querySelector("ol")
  const image = container.querySelector("[itemprop='image']")
  const name = container.querySelector("[itemprop='name']")
  const li = ol.querySelector("li")
  const liTemplate = li.cloneNode(true)
  ol.removeChild(li)
  const api = {
    container,
    ol,
    image,
    name,
    push(m){
      image.src = m.user.avatarUrl
      image.alt = `${m.user.username}'s avatar`
      name.innerHTML = m.user.username
      const li = liTemplate.cloneNode(true)
      li.innerHTML = m.body
      ol.insertBefore(li, ol.firstElementChild)
    },
    show(){
      container.style.display = "block"
    },
    hide(){
      container.style.display = "none"
    },
    isFirst: true
  }
  return api
}

const ListOfMessagesView = (container, model, delegate) => {
  const speakerTemplate = container.querySelector("[itemtype='http://schema.org/Person']")
  const template = speakerTemplate.cloneNode(true)
  container.removeChild(speakerTemplate)
  let currentView = null
  const previousMessage = m => {
    if(m && m.length > 1) return m[m.length-2]
    return null
  }
  const putItOnTop = (parent, view)=>{
    parent.insertBefore(view.container, parent.firstElementChild)
  }
  const update = (key, old, v)=>{
    const p = previousMessage(model.messages)
    if(!p || p.user.id != v.user.id){
      currentView = SpeakerView(template.cloneNode(true))
      putItOnTop(container, currentView)
    }
    currentView.push(v)
  }
  model.messages.observe("push", update)
}

export default ListOfMessagesView

import MakeObservableList from "./MakeObservableList.mjs"
import ListOfMessagesView from "./ListOfMessagesView.mjs"
const views = []
const subscriptions = {}
let socket = null
const messages = MakeObservableList()
let input = null
const messageFromMe = (me, message) => {
  return me.id == message.user.id
}
const pipe = (...fns) => x => fns.reduce((y, f) => f(y), x)

const App = {
  init(window, io, user){
    this.user = user
    const form = window.document.getElementById("messageTextBox")
    form.querySelector("input[type='text']").focus()
    input = form.querySelector("[name='message']")
    form.addEventListener("submit", this.submit.bind(this))
    socket = io()
    socket.on("message from user", this.messageFromUser.bind(this))
    socket.on("room joined", this.joined.bind(this))
    this.views.push(ListOfMessagesView(window.document.getElementById("messages"), {user: user, messages: messages}, this))
    return this
  },
  joined(message){
    console.log("joined", message)
  },
  messageFromUser(message){
    console.log("mesagefromuser message from user", message)
    if(messageFromMe(this.user, message)) return
    messages.push(message)
  },
  submit(e){
    e.preventDefault()
    if(input.value.length == 0) return
    const m = {user: this.user, body: input.value}
    socket.emit("message from user", m)
    messages.push(m)
    input.value = ""
  },
  send(message){
    socket.emit("sending message from user", message)
  },
  get views(){
    return views
  }
}

export default App
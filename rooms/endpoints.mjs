const RoomsEndpoints = (app, model) => {
  app.get("/rooms/:room", async (req, res) => {
    let room = model.find(r=>r.name.toLowerCase() == req.params.room.trim().toLowerCase())
    if(!room) room = {name: "General"}
    res.format({
      html(){
        res.render("rooms/index", { layout: "layouts/chat.html", title: room.name, model: room, userJson: JSON.stringify(req.user)})
      },
      json(){
        res.json({model: room})
      }
    })
  })
}
export default RoomsEndpoints
const TemplateLiteral = {
  interpolate(template, ...args){
    return Function(...args, 'return `' + template + '`;')
  },
  compile(template, $ = "$"){
    return Function($, 'try { return `' + template + '`;} catch(err) { return err }')
  }
}
export default TemplateLiteral
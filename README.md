Chatty McChat Chat
-------------------------

This is a chat app. You can signin with Github or Twitter. Once authenticated, each room is a URI, `rooms/general`, `rooms/welcome`.

NOTICE: If you're viewing this in Glitch's iFrame, it clicking on Github or Twitter to login won't work. Gotta open this up in it's own window.


TODO
-------------------------
- Enable any room
- Show who's active/logged in
- style the UI

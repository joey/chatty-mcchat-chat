import path from "path"
import express from "express"
import fs from "fs"
import BodyParser from "body-parser"
import MethodOverride from "method-override"
import hbs from "hbs"
import HomeEndpoints from "./home/endpoints.mjs"
import MakeObservableList from "./object-communications/MakeObservableList.mjs"
import MakeOverridingRingBuffer from "./object-communications/MakeOverridingRingBuffer.mjs"
import Auth from "./auth/endpoints.mjs"
import compression from "compression"
import cookieParser from "cookie-parser"
import cookieSession from "cookie-session"
import http from "http"
import RoomsEndpoints from "./rooms/endpoints.mjs"
import socketIo from "socket.io"
import Url from "url"
import Chat from "./chat.mjs"

const moduleURL = new URL(import.meta.url)
const File = fs.promises
const app = express()
const __dirname = path.dirname(moduleURL.pathname)
const MESSAGES_FILE_NAME = "./.data/messages.json"
app.cookieParser = cookieParser()
app.cookieSession = cookieSession({
  keys: ["chatty-mcchat-chat", ":blarbityblarbblarb:"],
  secret: process.env.COOKIE_SECRET
})

express.static.mime.define({"text/javascript": ["js", "mjs"]})
app.use("/js", express.static(path.join(__dirname, "src")))
app.use("/socketIo", express.static(path.join(__dirname, "node_modules/socket.io-client/dist")))
app.use(BodyParser.urlencoded({ extended: false }))
app.use(BodyParser.json())
app.use(MethodOverride("_method"))
app.use(app.cookieParser)
app.use(app.cookieSession)
app.use((req, res, next)=>{
  res.setHeader("Content-Security-Policy", "default-src 'self' https://glitch.com; frame-src https://github.com")
  next()
})

//app.use("/css", express.static("css"))
//app.set("view", View)

app.engine("html", hbs.__express)
app.set("views", __dirname)
app.set("view engine", "html")
hbs.registerPartial("home/index", fs.readFileSync(path.resolve(__dirname, "home/index.html"), "utf-8"))
const writeToDisk = async () => {
  //await File.writeFile(MESSAGES_FILE_NAME, JSON.stringify(products))
}
let messages = MakeObservableList(MakeOverridingRingBuffer({max: 5000}))
messages = HomeEndpoints(app, messages)
// let data = null
// try{
//   data = fs.readFileSync(MESSAGES_FILE_NAME, "utf-8")
// }catch(e){console.error(e)}
// if(data) JSON.parse(data).forEach(m => messages.push(m))

const Persist = (key, old, value) => {
  console.log(key, value)
  writeToDisk().then().catch(err => console.error(err))
}
messages.observe("push", Persist)
messages.observe("remove", Persist)

app.get("/messages", (req, res) => {
  let message = null
  let current = []
  while(message = messages.shift()){
    if(current[current.length-1] === message) break
    current.push(message)
  }
  res.json(current)
})
let server = http.createServer(app).listen(process.env.PORT, () => {
  console.log(`server started on ${server.address().port}`)
})

//transport close
//ping timeout
const io = socketIo(server)
Auth(app, io, process.env)

const rooms = MakeObservableList()
rooms.push({name: "General"})
rooms.push({name: "Random"})
RoomsEndpoints(app, rooms)

io.on("connection", socket => {
  const room = Url.parse(socket.handshake.headers.referer).pathname.split("/").pop()
  socket.join(room)
  socket.emit("room joined", {user: socket.request.user, room: room})
  //console.log(socket.adapter.rooms)
  //console.log("connected\n", Object.keys(io.clients().connected).map(id=>io.clients().connected[id].rooms))    
  socket.on("disconnect", message=>{
    console.log("logged off", message)
    socket.leave(room)
  })
  socket.on("message from user", message => {
    console.log("message from user", message)
    io.to(room).emit("message from user", {user: message.user, body: message.body})
  })
})


const handler = async () => {
  console.log("shutting down")
  await writeToDisk()
  process.exit()
}

;["SIGINT", "SIGTERM"].forEach( sig => {
  process.on(sig, handler)
})



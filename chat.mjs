import uuid from "uuid"
import MakeObservableList from "./object-communications/MakeObservableList.mjs"
const App = (delegate)=>{
  delegate = delegate || {
    joined(user, roomName){},
    left(user, roomName){},
    messageSentToRoom(message, roomName){},
    messageSentTo(user, message){},
    messageSentToThread(message, user){}
  }
  const rooms = {}
  const messages = {}
  const users = MakeObservableList()
  return {
    join(user, roomName){
      if(!rooms[roomName]) rooms[roomName] = MakeObservableList()
      if(!messages[roomName]) messages[roomName] = MakeObservableList()
      rooms[roomName].push(user)
      delegate.joined(user, roomName)
    },
    leave(user, roomName){
      if(!rooms[roomName]) return
      rooms[roomName].remove(s=>s.id == user.id)
      delegate.left(user, roomName)
    },
    sendMessageToRoom(message, roomName){
      if(!messages[roomName]) return
      if(!rooms[roomName].some(s=>s.id == message.user.id)) return
      messages[roomName].push(message)
      for(let i = 0; i < rooms[roomName].length; i++){
        rooms[roomName][i].messages.push(message)
        delegate.messageSentToRoom(message, roomName)
      }
    },
    sendMessageTo(user, message){
      users.find(u=>user.id == u.id).messages.push(message)
      delegate.messageSentTo(user, message)
    },
    addUser(user){
      users.push(user)
    },
    sendMessageToThread(source, target){
      target.messages.push(source)
      delegate.messageSentToThread(source, target)
    },
    createARoom(roomName){
      if(rooms[roomName]) return
      rooms[roomName] = MakeObservableList()
    },
    get rooms(){
      return rooms
    },
    get users(){
      return users
    },
    get messages(){
      return messages
    }
  }
}
const Message = ({user, body})=>{
  const messages = MakeObservableList()
  const id = uuid.v4()
  return {
    get messages(){
      return messages
    },
    get id(){
      return id
    },
    get body(){
      return body
    },
    get user(){
      return user
    }
  }
}
const User = (u)=>{
  const username = u.username
  const avatarUrl = u.avatarUrl
  const id = u.id
  let messages = MakeObservableList()
  messages.from = user=>messages.filter(m=>m.user.id == user.id)
  return {
    get messages(){
      return messages
    },
    get username(){
      return username
    },
    get id(){
      return id
    },
    get avatarUrl(){
      return avatarUrl
    }
  }  
}

export default {App, User, Message}
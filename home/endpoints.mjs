
const HomeEndpoints = (app, model) => {

  app.get("/", async (req, res) => {
    res.format({
      html(){
        res.render("home/index", { layout: "layouts/default.html", title: "Home", model:model})
      },
      json(){
        res.json({model: model})
      }
    })
  })
  
  return model
}
export default HomeEndpoints
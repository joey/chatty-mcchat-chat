import tap from "tap"
import fs from "fs"
import path from "path"
import MakeObservableList from "../src/MakeObservableList.mjs"
import ListOfMessagesView from "../src/ListOfMessagesView.mjs"
import jsdom from "jsdom"

const File = fs.promises
const moduleURL = new URL(import.meta.url)
const __dirname = path.dirname(moduleURL.pathname)
const makeDom = data => {
    let c = new jsdom.VirtualConsole()
    c.sendTo(console)
    return new jsdom.JSDOM(data, {
        url: "https://example.org/",
        referrer: "https://example.com/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 10000000,
        pretendToBeVisual: true
    })
}
const getHtml = async () => {
  let data = null
  try {
      data = await File.readFile(path.join(__dirname, "../rooms/index.html"), {encoding: "utf-8"})
  } catch(e) {
      console.error(e)
  }
  return makeDom(data)
}

tap.test("Sending a message", async t => {
  let dom = await getHtml()
  let messages = MakeObservableList()
  let view = ListOfMessagesView(dom.window.document.getElementById("messages"), messages, {})
  messages.push({message: "hi"})
  const lis = dom.window.document.querySelectorAll("#messages li")
  t.equals(lis.length, 2, "The message should be appended to the list.")
  dom.window.close()
  t.end()
})

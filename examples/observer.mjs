import tap from "tap"
import jsdom from "jsdom"
import fs from "fs"
import path from "path"
import MakeObservableList from "../src/MakeObservableList.mjs"
import ListOfMessagesView from "../src/ListOfMessagesView.mjs"
const File = fs.promises
const moduleURL = new URL(import.meta.url)
const __dirname = path.dirname(moduleURL.pathname)

const makeDom = data => {
    let c = new jsdom.VirtualConsole()
    c.sendTo(console)
    return new jsdom.JSDOM(data, {
        url: "https://example.org/",
        referrer: "https://example.com/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 10000000,
        pretendToBeVisual: true
    })
}

const getHtml = async () => {
  let data = null
  try {
      data = await File.readFile(path.join(__dirname, "../rooms/index.html"), {encoding: "utf-8"})
  } catch(e) {
      console.error(e)
  }
  return makeDom(data)
}

tap.test("Clicking on a star turns all stars before it blue", async t => {
  let dom = await getHtml()
  let model = MakeObservableList()
  let view = ListOfMessagesView(dom.window.document.getElementById("messages"), model, {})
  model.push({message: "hi"})
  const lis = dom.window.document.querySelectorAll("#messages li")
  t.equals(lis.length, 2)
  dom.window.close()
  t.end()
})

tap.test("Should observe a change", t => {
  let model = MakeObservable({firstName: "joey", lastName: "guerra"})
  const observer = {
    update(key, old, value){
      model.name = `${model.firstName} ${model.lastName}`
    }
  }
  model.observe("firstName", observer)
  model.observe("lastName", observer)
  model.firstName = "notjoey"
  model.lastName = "notguerra"
  t.ok(model.name === `${model.firstName} ${model.lastName}`)
  t.end()
})
tap.test("a function can be an observer", t => {
  let observable = MakeObservable({name: "joey guerra", age: 23})
  observable.observe("name", (key, old, value) => {
    t.ok(value === "Bruce Banner")
    t.end()
  })
  observable.name = "Bruce Banner"
})

tap.test("should stop observing", t => {
  let observable = MakeObservable({name: "joey guerra", age: 23})
  const nameObserver = (key, old, value) => {
    t.ok(value === "Bruce Banner")
  }
  observable.observe("name", nameObserver)
  observable.name = "Bruce Banner"
  observable.stopObserving("name", nameObserver)
  observable.name = "back to joey"
  t.ok(observable.name === "back to joey")
  t.end()
})


import tap from "tap"
import MakeObservableList from "../object-communications/MakeObservableList.mjs"
import Chat from "../chat.mjs"

const joey = {id: 1234, username: "joey", avatarUrl: "https://avatar.com/1234"}
const jane = {id: 1235, username: "jane", avatarUrl: "https://avatar.com/1235"}
const jack = {id: 1236, username: "jack", avatarUrl: "https://avatar.com/1236"}

tap.test("Only users in a room can send messages to that room", async t => {
  const roomName = "general"
  const app = Chat.App()
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  app.join(user, roomName)
  app.join(user2, roomName)
  app.sendMessageToRoom({user: fromUser, body: "Hi"}, roomName)
  t.ok(user.messages.length == 0, "user shouldn't have any messages")
  t.ok(user2.messages.length == 0, "user 2 shouldn't have any messages")
  t.end()
})

tap.test("A user sends a message to a room and all users in the room get it", async t => {
  const roomName = "general"
  const app = Chat.App()
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  app.join(fromUser, roomName)
  app.join(user, roomName)
  app.join(user2, roomName)
  app.sendMessageToRoom({user: fromUser, body: "Hi"}, roomName)
  t.ok(fromUser.messages.length > 0, "fromUser should've gotten the message")
  t.ok(user.messages.length > 0, "user should've gotten the message")
  t.ok(user2.messages.length > 0, "user 2 should've gotten the message")
  t.equals(user.messages[0].body, "Hi")
  t.end()
})

tap.test("Someone joins a room and then leaves, message is sent and that person doesn't get the message", async t => {
  const roomName = "general"
  const app = Chat.App()
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  app.join(fromUser, roomName)
  app.join(user, roomName)
  app.join(user2, roomName)
  app.leave(user2, roomName)
  app.sendMessageToRoom({user: fromUser, body: "Hi"}, roomName)
  t.ok(user.messages.length > 0, "user should've gotten the message")
  t.ok(user2.messages.length == 0, "user 2 should NOT have gotten the message")
  t.equals(user.messages[0].body, "Hi")
  t.end()
})

tap.test("Send a private message to someone", t => {
  const app = Chat.App()
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  const expected = "This should be private"
  app.addUser(fromUser)
  app.addUser(user)
  app.addUser(user2)
  app.sendMessageTo(user, {user: fromUser, body: expected})
  t.equals(user.messages.from(fromUser)[0].body, expected)
  t.equals(user2.messages.length, 0)
  t.end()
})

tap.test("Messages can have messages, threading", t => {
  const roomName = "general"
  const app = Chat.App()
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  const message = Chat.Message({user: fromUser, body: "Hi"})
  app.join(fromUser, roomName)
  app.join(user, roomName)
  app.join(user2, roomName)
  app.sendMessageToRoom(message, roomName)
  app.sendMessageToThread(Chat.Message({user: user, body: "I know right?"}), message)
  app.sendMessageToThread(Chat.Message({user: fromUser, body: "Yes, and i'm sick of it!"}), message)
  t.equals(message.messages[0].body, "I know right?")
  t.equals(message.messages[1].body, "Yes, and i'm sick of it!")
  t.end()
})

tap.test("Get notified when someone joins a room", t => {
  const roomName = "general"
  const app = Chat.App()
  app.createARoom(roomName)
  app.rooms[roomName].observe("push", (key, old, v)=>{
    t.equals(v.id, fromUser.id)
    t.end()
  })
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  const message = Chat.Message({user: fromUser, body: "Hi"})
  app.join(fromUser, roomName)
})

tap.test("get notified when someone leaves a room", t => {
  const roomName = "general"
  const app = Chat.App()
  app.createARoom(roomName)
  app.rooms[roomName].observe("remove", (key, old, v)=>{
    t.equals(old.id, fromUser.id)
    t.end()
  })
  const fromUser = Chat.User(joey)
  const user = Chat.User(jane)
  const user2 = Chat.User(jack)
  const message = Chat.Message({user: fromUser, body: "Hi"})
  app.join(fromUser, roomName)
  app.leave(fromUser, roomName)
})